package ru.denis.projectBooks;

public class Book {
    private String book;
    private String author;
    private int date;
    private String url;
    private Genre genre;


    Book(String book, String author, int date, String url, Genre genre) {
        this.book = book;
        this.author = author;
        this.date = date;
        this.url = url;
        this.genre = genre;

    }

    String getBook() {
        return book;
    }

    String getAuthor() {
        return author;
    }

    int getDate() {
        return date;
    }

    public String getUrl() {
        return url;
    }

    public Genre getGenre() {
        return genre;
    }

    @Override
    public String toString() {
        return "библиотека:" +
                "Книга - " + book +
                ", Автор - " + author +
                ", Год издания - " + date +
                ", Ссылка - " + url +
                ", Жанр - " + genre +
                ')';
    }
}
