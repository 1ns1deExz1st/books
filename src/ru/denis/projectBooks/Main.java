package ru.denis.projectBooks;

import java.util.ArrayList;
import java.util.Arrays;

    /**
     * В классе Main созданы книги и вызваны методы которые сортируют книги по определенным параметрам.
     *
     * @author Denis Leontev.
     */
public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Трава", "Чуковсий", 2020, "С:книга1", Genre.FANTASY);
        Book book2 = new Book("Доктор", "Пушкин", 2021, "С:книга2", Genre.HORRORS);
        Book book3 = new Book("Земля", "Орлов", 2000, "С:книга3", Genre.DOCUMENTARY);
        Book book4 = new Book("Аист", "Лягушкин", 1999, "С:книга4", Genre.FIGHTER);
        Book book5 = new Book("Гном", "Подземный", 1892, "С:книга5", Genre.MELODRAMA);
        Book book6 = new Book("Инфляция", "Зарёв", 3123, "С:книга6", Genre.HORRORS);

        ArrayList<Book> books = new ArrayList<>(Arrays.asList(book1, book2,book3,book4,book5,book6));
        System.out.println();
        print(bookSorting(books));
        print(authorSorting(books));
        print(birthYearSorting(books));
    }

    /**
     * Метод authorSorting сортирует книги по авторам.
     *
     * @param arrayList - передаём в метод ArrayList для дальнейшей обработки в методе сортировки.
     * @return - возвращаем ArrayList.
     * @author - Denis Leontev.
     */
    private static ArrayList authorSorting(ArrayList<Book> arrayList) {
        Book temp;

        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 1 + i; j < arrayList.size(); j++) {
                int compare = arrayList.get(j).getAuthor().compareTo(arrayList.get(i).getAuthor());
                if (compare < 0) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }

                if (compare == 1) {
                    if (arrayList.get(j).getDate() < arrayList.get(i).getDate()) {
                        temp = arrayList.get(i);
                        arrayList.set(i, arrayList.get(j));
                        arrayList.set(j, temp);
                    }
                }
            }
        }
        return arrayList;
    }
        private static ArrayList bookSorting(ArrayList<Book> arrayList) {
            Book temp;

            for (int i = 0; i < arrayList.size(); i++) {
                for (int j = 1 + i; j < arrayList.size(); j++) {
                    int compare = arrayList.get(j).getBook().compareTo(arrayList.get(i).getBook());
                    if (compare < 0) {
                        temp = arrayList.get(i);
                        arrayList.set(i, arrayList.get(j));
                        arrayList.set(j, temp);
                    }

                    if (compare == 1) {
                        if (arrayList.get(j).getDate() < arrayList.get(i).getDate()) {
                            temp = arrayList.get(i);
                            arrayList.set(i, arrayList.get(j));
                            arrayList.set(j, temp);
                        }
                    }
                }
            }
            return arrayList;
        }
    /**
     * Метод birthYerSorting сортирует книги по авторам.
     *
     * @param arrayList - передаём в метод ArrayList для дальнейшей обработки в методе сортировки.
     * @return - возвращаем ArrayList.
     * @author - Denis Leontev.
     */
    private static ArrayList birthYearSorting(ArrayList<Book> arrayList) {
        Book temp;

        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = i + 1; j < arrayList.size(); j++) {
                if (arrayList.get(j).getDate() < arrayList.get(i).getDate()) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }
            }
        }
        return arrayList;
    }

    /**
     * Метод нормального вывода отсортированных данных.
     *
     * @param arrayList - передаём в метод ArrayList для дальнейшей обработки в методе сортировки.
     * @author - Denis Leontev.
     */
    private static void print(ArrayList<Book> arrayList) {
        for (Book book : arrayList) {

            System.out.println(book);
        }
        System.out.println();
    }
}
